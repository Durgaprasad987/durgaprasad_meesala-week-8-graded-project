import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-completed-books',
  templateUrl: './completed-books.component.html',
  styleUrls: ['./completed-books.component.css']
})
export class CompletedBooksComponent implements OnInit {

  completedBookData !: any;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.completedBooks();
  }
  completedBooks() {
    this.api.getCompletedBook()
      .subscribe(res => {
        this.completedBookData = res;
      })
  }

}

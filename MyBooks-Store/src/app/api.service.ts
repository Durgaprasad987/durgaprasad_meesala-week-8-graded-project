import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http:HttpClient) { }

  postBook(data:any){
    return this.http.post<any>("http://localhost:3000/bookstore",data).pipe(map((res:any)=>{
      return res;
    }))
  }

  getBook(){
    return this.http.get<any>("http://localhost:3000/bookstore").pipe(map((res:any)=>{
      return res;
    }))
  }

  updateBook(data:any,id:number){
    return this.http.put<any>("http://localhost:3000/bookstore/"+id,data)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  deleteBook(id :number){
    return this.http.delete<any>("http://localhost:3000/bookstore/"+id)
    .pipe(map((res:any)=>{
      return res;
    }))
  }
  wishlistBook(data:any){
    return this.http.post<any>("http://localhost:3000/wishlist", data)
    .pipe(map((res:any) => {
      return res;
    }))
  }
  getWishlistedBook(){
    return this.http.get<any>("http://localhost:3000/wishlist")
    .pipe(map((res:any)=>{
      return res;
    }))
  }
 completedBook(data:any){
    return this.http.post<any>("http://localhost:3000/completedBooks", data)
    .pipe(map((res:any) => {
      return res;
    }))
  }
  getCompletedBook(){
    return this.http.get<any>("http://localhost:3000/completedBooks")
    .pipe(map((res:any)=>{
      return res;
    }))
  }







}

import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  bookData !: any;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.getAllBooks() ;
  }
  getAllBooks() {
    this.api.getBook()
      .subscribe(res => {
        this.bookData = res;
      })

}

}

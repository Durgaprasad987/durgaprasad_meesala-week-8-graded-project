import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public loginForm !:FormGroup;

  constructor(private formBuilder:FormBuilder,private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.loginForm=this.formBuilder.group({
      email:['',Validators.required],
      password:['',Validators.required],
      })
  }
  Login(){
    this.http.get<any>("http://localhost:3000/registerUsers")
    .subscribe(res=>{
      const user =res.find((a:any)=>{
        return a.emailadd === this.loginForm.value.email && a.password === this.loginForm.value.password
      });
      if(user){
        alert("Login SuccessFull!!");
        this.loginForm.reset();
        this.router.navigate(['user-home'])
      }else{
        alert("user not found!!");
      }
    },err=>{
      alert("something went wrong!!")
    })
  }


}

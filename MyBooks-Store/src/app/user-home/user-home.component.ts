import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ApiService } from '../api.service';
import { BookModel } from '../bookmodel';

@Component({
  selector: 'app-user-home',
  templateUrl: './user-home.component.html',
  styleUrls: ['./user-home.component.css']
})
export class UserHomeComponent implements OnInit {

  formValue !: FormGroup; 
  bookModelObj: BookModel = new BookModel(); 
  bookData !: any; 
  showAdd !:boolean;
  showUpdate !:boolean;

  constructor(private Formbuilder: FormBuilder, private api: ApiService) { }

  ngOnInit(): void {
    this.formValue = this.Formbuilder.group({
      BookID: [''],
      BookName: [''],
      Author: [''],
      Price: [''],
    })
    this.getAllBooks();
  }

  postBookDetails() {
    this.bookModelObj.BookID = this.formValue.value.BookID;
    this.bookModelObj.BookName = this.formValue.value.BookName;
    this.bookModelObj.Author = this.formValue.value.Author;
    this.bookModelObj.Price = this.formValue.value.Price;

    this.api.postBook(this.bookModelObj)
      .subscribe(res => {
        console.log(res);
        alert("Book Added Succesfully");
        let ref = document.getElementById('cancel')
        ref?.click();
        this.formValue.reset();
        this.getAllBooks();
      },
        err => {
          alert("Something went wrong")
        })
  }

  clickAddBook(){
    this.formValue.reset();
    this.showAdd=true;
    this.showUpdate=false;
  }
  
  getAllBooks() {
    this.api.getBook()
      .subscribe(res => {
        this.bookData = res;
      })
  }

  deleteBook(value: any) {
    this.api.deleteBook(value.id)
      .subscribe(res => {
        alert("Book Removed");
        this.getAllBooks();
       
      })
  }

  onEdit(value: any) {
    this.showAdd=false;
    this.showUpdate=true;
    this.bookModelObj.id=value.id;
    this.formValue.controls['BookID'].setValue(value.BookID);
    this.formValue.controls['BookName'].setValue(value.BookName);
    this.formValue.controls['Author'].setValue(value.Author);
    this.formValue.controls['Price'].setValue(value.Price);
  }
  updateBookDetails() { 
    this.bookModelObj.BookID = this.formValue.value.BookID;
    this.bookModelObj.BookName = this.formValue.value.BookName;
    this.bookModelObj.Author = this.formValue.value.Author;
    this.bookModelObj.Price = this.formValue.value.Price;

    this.api.updateBook(this.bookModelObj,this.bookModelObj.id)
    .subscribe(res=>{
      alert("Updated Succesfully");
      let ref = document.getElementById('cancel')
        ref?.click();
        this.formValue.reset();
        this.getAllBooks();
    })
  }
  wishlistBook(value:any){
    this.bookModelObj.id = value.id;
    this.bookModelObj.BookID = value.BookID;
    this.bookModelObj.BookName = value.BookName;
    this.bookModelObj.Author = value.Author;
    this.bookModelObj.Price = value.Price;

    this.api.wishlistBook(this.bookModelObj)
    .subscribe(res => {
      alert("Wishlisted Book Successfully");
      this.getAllBooks();
    })
  }
  completedBook(value:any){
    this.bookModelObj.id = value.id;
    this.bookModelObj.BookID = value.BookID;
    this.bookModelObj.BookName = value.BookName;
    this.bookModelObj.Author = value.Author;
    this.bookModelObj.Price = value.Price;

    this.api.completedBook(this.bookModelObj)
    .subscribe(res => {
      alert("Book added under Completed list Successfully");
      this.getAllBooks();
    })
  }

}

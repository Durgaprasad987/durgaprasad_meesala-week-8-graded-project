import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';

@Component({
  selector: 'app-wishlist-books',
  templateUrl: './wishlist-books.component.html',
  styleUrls: ['./wishlist-books.component.css']
})
export class WishlistBooksComponent implements OnInit {

  wishlistData !:any;
  constructor(private api: ApiService) { }

  ngOnInit(): void {
    this.wishlistedBooks();
  }

  wishlistedBooks() {
    this.api.getWishlistedBook()
      .subscribe(res => {
        this.wishlistData = res;
      })

}

}

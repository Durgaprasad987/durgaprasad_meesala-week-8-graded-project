import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CardviewComponent } from './cardview/cardview.component';
import { CompletedBooksComponent } from './completed-books/completed-books.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { RegistrationComponent } from './registration/registration.component';
import { WishlistBooksComponent } from './wishlist-books/wishlist-books.component';

const routes: Routes = [
  {path:'', redirectTo:'home-page',pathMatch:'full'},
  {path:'home-page' , component:HomeComponent},
  {path:'login-page' , component:LoginComponent},
  {path:'registration-page' , component:RegistrationComponent},
  {path:'user-home', component:CardviewComponent},
  {path:'wishlist', component:WishlistBooksComponent},
  {path:'completedbooks',component:CompletedBooksComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

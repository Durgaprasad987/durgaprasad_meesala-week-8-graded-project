import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  public registerForm !:FormGroup;

  constructor(private formBuilder:FormBuilder,private http:HttpClient,private router:Router) { }

  ngOnInit(): void {
    this.registerForm=this.formBuilder.group({
      
      firstname:['',Validators.required],
      lastname:['',Validators.required],
      emailadd:['',Validators.required],
      password:['',Validators.required],
    })
  }

  Register(){
    this.registerForm.value.id = 0;
    console.log(this.registerForm.value);

    this.http.post<any>("http://localhost:3000/registerUsers",this.registerForm.value)
    .subscribe(res=>{
      alert("Registration SuccessFull");
      this.registerForm.reset();
      this.router.navigate(['login-page']);
    },err=>{
      console.log(err);
      alert("Something Went Wrong");
    })
}

}
